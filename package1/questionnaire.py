import copy
# from tools import say


class Questionnaire():
    questions = []

    def __init__(self, title):
        self.title = title

    def set_questions(self, questions):
        self.questions = copy.deepcopy(questions)

    def get_questions(self):
        return self.questions
