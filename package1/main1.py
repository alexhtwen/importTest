# import module1
import path
from tools import say


# 問題是：VS Code的自動編排機制會無法保持這個順序。Jupyter Notebook則OK。
# from path import set_path
# set_path('/home/alex/Python/lib/')

# set_path()須先，from tools import say須後，順序重要。
# from tools import say

say('Bingo')
