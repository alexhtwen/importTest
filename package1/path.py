import sys
# 以下這列是可執行的code，看來不可避免。
sys.path.append("/home/alex/Python/lib/")


# 如果硬要將path.py做成「嚴格」的module，就要包成以下函數：
def set_path(path):
    import sys
    sys.path.append(path)
