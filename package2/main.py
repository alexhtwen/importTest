# import package1
from package2.subpackage1.abc import title

print()
print(title)
print()

# import package1.subpackage1.subsubpackage1.module1


# from .package1 import module1
# from module0 import function0
# from package1.module2 import function1
# from package2 import Class1
# from package2.subpackage1.module5 import function2


# function1()
# Class1()
# my_class = Class1()
# function2()
# function0()
